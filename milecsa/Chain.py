from milecsa.Rpc import Rpc


class Chain:

    __response__ = None

    def __init__(self):
        if not Chain.__response__:
            rpc = Rpc("get-blockchain-info", params={})
            Chain.__response__ = rpc.exec()
        self.project = Chain.__response__.result['project']
        self.version = Chain.__response__.result['version']
        self.supported_transactions = Chain.__response__.result['supported_transactions']
        self.supported_assets = Chain.__response__.result['supported_assets']
        self.asset_codes = dict(map(lambda a: (a['code'], a['name']), self.supported_assets))
        self.asset_names = dict(map(lambda a: (a['name'], a['code']), self.supported_assets))

    def asset_code(self, name):
        return self.asset_names[name]

    def asset_name(self, code):
        return self.asset_codes[code]

    def __eq__(self, other):
        return (self.project == other.project) and (self.version == other.version)
