from milecsa.Rpc import Rpc
from milecsa.Chain import Chain


class Transaction:

    data = None
    wallet = None
    id = None

    def __init__(self, wallet, id):
        self.wallet = wallet
        self.id = str(id)
        self.__chain = Chain()

    def send(self):
        self.build()
        rpc = Rpc("send-signed-transaction", params={"transaction_data": self.data})
        response = rpc.exec()
        return response.result

    def build(self):
        pass