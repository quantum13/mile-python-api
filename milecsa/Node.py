from __milecsa import __register_node as register_node, __unregister_node as unregister_node
from milecsa import Wallet, Transaction

class Node(Transaction):

    address = None

    def __init__(self, wallet, address, id="0"):

        Transaction.__init__(self, wallet=wallet, id=id)

        self.address = address

    def register(self):
        return register_node(self.wallet.publicKey, self.wallet.privateKey, self.address, self.id)

    def unregister(self):
        return unregister_node(self.wallet.publicKey, self.wallet.privateKey, self.id)
