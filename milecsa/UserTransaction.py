from __milecsa import __transaction_user_data as transaction_user_data
from milecsa import Transaction


class UserTransaction(Transaction):
    userData = None

    def __init__(self, wallet, data, id="0"):
        Transaction.__init__(self, wallet=wallet, id=id)
        self.userData = data

        self.data = transaction_user_data(self.wallet.publicKey, self.wallet.privateKey, self.userData, self.id)
