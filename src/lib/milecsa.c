#include <Python.h>
#include <stdio.h>
#include "../api/wrapper/c/csa_cxx_light.h"

static PyObject *PyErr_MileCsaError;

static PyObject* py_generate_key_pair(PyObject *self, PyObject *args)
{
    pcsa_keys_pair keyPair;
    char* errorDescription = NULL;


    if (generate_key_pair(&keyPair, &errorDescription))
    {
        if (errorDescription)
        {
            PyErr_SetString(PyErr_MileCsaError, errorDescription);
            free(errorDescription);
        }
        return NULL;
    }

    return Py_BuildValue("{ssss}", "public_key", keyPair.public_key, "private_key", keyPair.private_key);
}

static PyObject* py_generate_key_pair_with_secret_phrase(PyObject *self, PyObject *args)
{
    pcsa_keys_pair keyPair;
    char* errorDescription = NULL;
    char *phrase;

    if (!PyArg_ParseTuple(args, "s", &phrase)) {
      PyErr_SetString(PyErr_MileCsaError, "secret phrase must be in args");
      return NULL;
    }

    if (generate_key_pair_with_secret_phrase(&keyPair, phrase, strlen(phrase), &errorDescription))
    {
        if (errorDescription)
        {
            PyErr_SetString(PyErr_MileCsaError, errorDescription);
            free(errorDescription);
        }
        return NULL;
    }

    return Py_BuildValue("{ssss}", "public_key", keyPair.public_key, "private_key", keyPair.private_key);
}

static PyObject* py_generate_key_pair_from_private_key(PyObject *self, PyObject *args)
{
    pcsa_keys_pair keyPair;
    char* errorDescription = NULL;
    char *private_key;

    if (!PyArg_ParseTuple(args, "s", &private_key)) {
      PyErr_SetString(PyErr_MileCsaError, "secret phrase must be in args");
      return NULL;
    }

    memcpy(keyPair.private_key, private_key, sizeof(keyPair.private_key));

    if (generate_key_pair_from_private_key(&keyPair, &errorDescription))
    {
        if (errorDescription)
        {
            PyErr_SetString(PyErr_MileCsaError, errorDescription);
            free(errorDescription);
        }
        return NULL;
    }

    return Py_BuildValue("{ssss}", "public_key", keyPair.public_key, "private_key", keyPair.private_key);
}


static PyObject* py_create_transaction_transfer_assets(PyObject *self, PyObject *args)
{
    pcsa_keys_pair keyPair;
    char* errorDescription = NULL;
    char *dest;
    char *pk;
    char *pvk;
    char *amount;
    char *transactionId;
    char *transaction;
    int  asset;

    if (!PyArg_ParseTuple(args, "ssssis", &pk, &pvk, &dest, &transactionId, &asset, &amount)) {
      PyErr_SetString(PyErr_MileCsaError, "not enough args");
      return NULL;
    }


    memcpy(keyPair.public_key, pk, sizeof(keyPair.public_key));
    memcpy(keyPair.private_key, pvk, sizeof(keyPair.private_key));

    if (create_transaction_transfer_assets(&keyPair,
                                            dest,
                                            transactionId,
                                            (unsigned short)asset,
                                             amount,
                                             &transaction,
                                             &errorDescription))
    {
        if (errorDescription)
        {
            PyErr_SetString(PyErr_MileCsaError, errorDescription);
            free(errorDescription);
        }
        return NULL;
    }

    PyObject *o = Py_BuildValue("s", transaction);
    free(transaction);
    return o;
}

static PyObject* py_create_transaction_register_node(PyObject *self, PyObject *args)
{
    pcsa_keys_pair keyPair;
    char* errorDescription = NULL;
    char *nodeAddress;
    char *pk;
    char *pvk;
    char *transactionId;
    char *transaction;

    if (!PyArg_ParseTuple(args, "ssss", &pk, &pvk, &nodeAddress, &transactionId)) {
      PyErr_SetString(PyErr_MileCsaError, "not enough args");
      return NULL;
    }

    memcpy(keyPair.public_key, pk, sizeof(keyPair.public_key));
    memcpy(keyPair.private_key, pvk, sizeof(keyPair.private_key));

    if (create_transaction_register_node(&keyPair,
                                            nodeAddress,
                                            transactionId,
                                            &transaction,
                                            &errorDescription))
    {
        if (errorDescription)
        {
            PyErr_SetString(PyErr_MileCsaError, errorDescription);
            free(errorDescription);
        }
        return NULL;
    }

    PyObject *o = Py_BuildValue("s", transaction);
    free(transaction);
    return o;
}


static PyObject* py_create_transaction_unregister_node(PyObject *self, PyObject *args)
{
    pcsa_keys_pair keyPair;
    char* errorDescription = NULL;
    char *pk;
    char *pvk;
    char *transactionId;
    char *transaction;

    if (!PyArg_ParseTuple(args, "sss", &pk, &pvk, &transactionId)) {
      PyErr_SetString(PyErr_MileCsaError, "not enough args");
      return NULL;
    }

    memcpy(keyPair.public_key, pk, sizeof(keyPair.public_key));
    memcpy(keyPair.private_key, pvk, sizeof(keyPair.private_key));

    if (create_transaction_unregister_node(&keyPair,
                                            transactionId,
                                            &transaction,
                                            &errorDescription))
    {
        if (errorDescription)
        {
            PyErr_SetString(PyErr_MileCsaError, errorDescription);
            free(errorDescription);
        }
        return NULL;
    }

    PyObject *o = Py_BuildValue("s", transaction);
    free(transaction);
    return o;
}

static PyObject* py_create_transaction_user_data(PyObject *self, PyObject *args)
{
    pcsa_keys_pair keyPair;
    char* errorDescription = NULL;
    char *dataString;
    char *pk;
    char *pvk;
    char *transactionId;
    char *transaction;

    if (!PyArg_ParseTuple(args, "ssss", &pk, &pvk, &dataString, &transactionId)) {
      PyErr_SetString(PyErr_MileCsaError, "not enough args");
      return NULL;
    }

    memcpy(keyPair.public_key, pk, sizeof(keyPair.public_key));
    memcpy(keyPair.private_key, pvk, sizeof(keyPair.private_key));


    if (create_transaction_user_data(&keyPair,
                                            transactionId,
                                            dataString,
                                            &transaction,
                                            &errorDescription))
    {
        if (errorDescription)
        {
            PyErr_SetString(PyErr_MileCsaError, errorDescription);
            free(errorDescription);
        }
        return NULL;
    }

    PyObject *o = Py_BuildValue("s", transaction);
    free(transaction);
    return o;
}


static PyMethodDef milecsaMethods[] = {
    {"__key_pair", py_generate_key_pair, METH_NOARGS, "Mile"},
    {"__key_pair_with_secret_phrase", py_generate_key_pair_with_secret_phrase, METH_VARARGS, "Mile"},
    {"__key_pair_from_private_key", py_generate_key_pair_from_private_key, METH_VARARGS, "Mile"},
    {"__transfer_assets", py_create_transaction_transfer_assets, METH_VARARGS, "Mile"},
    {"__register_node", py_create_transaction_register_node, METH_VARARGS, "Mile"},
    {"__unregister_node", py_create_transaction_unregister_node, METH_VARARGS, "Mile"},
    {"__transaction_user_data", py_create_transaction_user_data, METH_VARARGS, "Mile"},
    {NULL, NULL, 0, NULL}
};

static struct PyModuleDef milecsa =
{
    PyModuleDef_HEAD_INIT,
    "__milecsa",   /* name of module */
    "",          /* module documentation, may be NULL */
    -1,          /* size of per-interpreter state of the module, or -1 if the module keeps state in global variables. */
    milecsaMethods
};

PyMODINIT_FUNC PyInit___milecsa(void)
{

    PyObject *m;

    m = PyModule_Create(&milecsa);
    if (m == NULL)
        return NULL;

    PyErr_MileCsaError = PyErr_NewException("milecsa.error", NULL, NULL);
    Py_INCREF(PyErr_MileCsaError);
    PyModule_AddObject(m, "error", PyErr_MileCsaError);

    return m;
}
