import __milecsa as milecsa


def main():
    print(dir(milecsa))
    print()

    try:
        print("1.0 Generate key")
        pk = milecsa.__key_pair()
        print(pk)

        print()

        print("1.1 Generate public key by private")

        pkp = pk['private_key']
        print(milecsa.__key_pair_from_private_key(pkp))

    except milecsa.error as error:
        print(error)


    print()

    try:

        print("2. Generate key by secret phrase")
        pk1 = milecsa.__key_pair_with_secret_phrase("The some phrase")
        pk2 = milecsa.__key_pair_with_secret_phrase("The some phrase")

        pk3 = milecsa.__key_pair_with_secret_phrase("The some new phrase")

        print(pk1, pk1 == pk2, pk1 == pk3)

    except milecsa.error as error:
        print(error)


    print()

    try:

        print("3. Create signed transfer transaction")
        print(milecsa.__transfer_assets(pk1['public_key'], pk1['private_key'], pk3["public_key"], "0", 1, "20"))

        print()

        print("4. Generate signed register node transaction")
        print(milecsa.__register_node(pk1['public_key'], pk1['private_key'], "http://karma.red", "0"))

        print()

        print("5. Generate signed unregister node transaction")
        print(milecsa.__unregister_node(pk1['public_key'], pk1['private_key'], "0"))

    except milecsa.error as error:
        print(error)


    print()

    try:
        print("6. User data transaction")

        print(milecsa.__transaction_user_data(pk1['public_key'], pk1['private_key'], '{k:"data",i:20,desc:"Some user can send transaction"}', "20"))
    except milecsa.error as error:
        print(error)

    print()

    try:
        print("7. Test exception")

        print(milecsa.__transfer_assets(pk1['public_key'],'-', pk3["public_key"], "0", 1, "20"))
    except milecsa.error as error:
        print(error)

if __name__ == "__main__":
    main()
