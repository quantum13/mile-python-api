from milecsa import Wallet, UserTransaction


def main():

    wallet = Wallet()

    data0 = UserTransaction(wallet=wallet, data='{k:"data",i:20,desc:"Some user can send transaction"}')
    data1 = UserTransaction(wallet=wallet, data='{k:"data",i:20,desc:"Some another data"}', id="10")

    print(data0.data, data0.userData)
    print(data1.data, data1.userData)


if __name__ == "__main__":
    main()
