from milecsa import Wallet, Node
import unittest

class MyTestCase(unittest.TestCase):
    def test_something(self):
        wallet = Wallet()
        wallet1 = Wallet()

        node0 = Node(wallet=wallet, address="https://karma.red")
        node1 = Node(wallet=wallet1, address="https://mile.global")

        r0 = node0.register()
        r1 = node1.register()
        print(r0)
        print(r1)

        print(node0.unregister())
        print(node1.unregister())

        self.assertEqual(r0 == r1, False)

if __name__ == '__main__':
    unittest.main()
